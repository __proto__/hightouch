# Hightouch Docs Site Demo

Deployed with Vercel, CI based on the `main` branch of this here bitbucket repo:
[https://hightouch-seven.vercel.app/](https://hightouch-seven.vercel.app/).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Screenshots
1440px
![](https://cloudup.com/iviTnjIL-2k+)

800px
![](https://cloudup.com/iTtGr9EF_kZ+)

390px (iPhone 11/12/13)
![](https://cloudup.com/iBMZtBizX89+)

Mobile Nav Modal Open
![](https://cloudup.com/ic49IwBv88H+)

## Next Steps for development

Connect buttons and nav to routes and pages/screens
Extract useNav hook
Further Customize Tachyons or Tailwind
Plan out SVG usage (colors)

## Architecture and Coding Style
Some component extraction has begun, but I didn't systematize too much.
The Modal Nav state persists across screen states.

Tachyons and tailwind, functional CSS for humans, can be jarring the first few encounters. 

A few conventions I try to follow to make it more legible and beneficial. 

0. Think about the box model.
   
1. Use composition. In the final class(Name), order from outside to inside, generally meaning:
   - display, height, width, border, margin, padding, contents, fonts, hovers (recursive), animations, etc. 
   
2. Write for mobile devices first, like `db dn-l` to show something only on "large screens" and `ma2 ma4-ns` (not small) to use more padding above mobile.
   
3. Use template strings to compose groups of classes, display,... ,animations, etc.

4. Use an array; push or filter to add and remove classes based on props or state.
   `<div class={['someClass', 'another-class', ...classGroup.filter(c => c !== 'excludedClass') ].join(' ')}>`
   
5. Layer divs to separate concerns.
   
The benefits are felt after using it for a while. 
Tachyons source is easy to read and customize. 
TailwindCSS is really comprehensive and makes it super easy to further customize or use off-the-shelf components.
https://tailwindcss.com/docs/utility-first
> Now I know what you’re thinking, “this is an atrocity, what a horrible mess!” and you’re right, it’s kind of ugly. In fact it’s just about impossible to think this is a good idea the first time you see it — you have to actually try it.
