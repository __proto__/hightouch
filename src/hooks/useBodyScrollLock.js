import React from "react";

export const useBodyScrollLock = (modalOpen) => {
  React.useLayoutEffect(() => {
    const originalStyle = window.getComputedStyle(document.body).overflow
    document.body.style.overflow = modalOpen ? 'hidden' : originalStyle
    return () => (document.body.style.overflow = originalStyle)
  }, [modalOpen])
}
