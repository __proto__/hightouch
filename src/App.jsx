import React from 'react'
import book from './svgs/book.svg'
import { NavBar } from "./components/Nav/Nav";
import { Articles } from "./components/Articles.jsx";
import { useBodyScrollLock } from "./hooks/useBodyScrollLock";
import { Header } from "./components/Header";
import { ModalNav } from "./components/Nav/ModalNav";

const PrimaryLayout = ({ children }) =>
  <div className="flex flex-column justify-between bg-white sans-serif">
    {children}
  </div>

const App = () => {
  const [navOpen, setNav] = React.useState(false)
  const toggleNav = () => setNav(prevState => !prevState)
  useBodyScrollLock(navOpen)

  const [activeItem, setActiveItem] = React.useState('')

  return (
    <PrimaryLayout>
      <Header toggleNav={toggleNav}/>

      <main className='flex'>
        <section className='dn db-l' id="Tray">
          <NavBar activeItem={activeItem} setActiveItem={setActiveItem}/>
        </section>

        <section className='pa3 pa4-l'>
          <img className="mb2" src={book} alt="book"/>
          <h1 className="mt3 f3 fw8">Hightouch Documentation</h1>
          <p className="mt3 gray">Get an overview of Hightouch’s features, integrations, and how to use them.</p>
          <p className="mt4 mb3 pt3 pb2 f5 fw6">Dive deeper...</p>
          <Articles/>
        </section>
      </main>

      <ModalNav navOpen={navOpen} toggleNav={toggleNav} activeItem={activeItem} setActiveItem={setActiveItem}/>
    </PrimaryLayout>
  )
}

export default App;
