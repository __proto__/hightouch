import React from "react";

const button = 'ba br2 bw1 pv2 ph3 fw5 pointer grow shadow-hover'

export const SignUp = ({ extraClass = '' }) => {
  const signUp = `${button} mr2 b--light-gray bg-white light-silver ${extraClass}`

  return <button className={signUp}>Sign up</button>
}
export const Login = ({ extraClass = '' }) => {
  const login = `${button} b--navy bg-navy near-white ${extraClass}`

  return <button className={login}>Login</button>
}
