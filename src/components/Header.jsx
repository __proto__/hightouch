import navBars from "../svgs/nav-bars.svg";
import React from "react";
import { Login, SignUp } from "./Buttons";

export const Header = ({ toggleNav }) => (
  <header className="ba bw1 b--light-gray pv3 ph3 ph4-l flex justify-between items-center">
    <div>
      <span className="near-black b">hightouch</span>
      <span className="light-gray"> | </span>
      <span className="light-silver b">docs</span>
    </div>

    <div className="dn-l">
      <div className="pointer" onClick={toggleNav}>
        <img src={navBars} className="" alt="navigation toggle"/>
      </div>
    </div>

    <div className="dn db-l">
      <SignUp/>
      <Login/>
    </div>
  </header>
)
