import React from "react";
import { navItems } from'./NAV_ITEMS'

const NavItem = ({ activeItem, setActiveItem, item: { svg, label } }) => {
  const hovers = `hover-bg-near-white pointer`
  const li = `br2 pa3 flex f6 ${hovers}`

  const activeTheme = `bg-near-white purple`
  const inactiveTheme = `near-black`

  const theme = activeItem === label ? activeTheme : inactiveTheme

  return (
    <li className={`${li} ${theme}`} onClick={() => setActiveItem(label)}>
      <img className="mr2" alt="nav icon" src={svg}/>
      {label}
    </li>
  )
}

export const NavBar = ({ navOpen, toggleNav, activeItem, setActiveItem }) => {
  const handleClick = () => {
    navOpen && toggleNav()
  }

  const large = 'h-100-l w5-l bn br-l bw1-l b--light-gray'
  const small = 'ml3 pv3 pr3'

  return (
    <nav className={`${small} ${large}`}>
      <ul className='list' onClick={handleClick}>
        {navItems.map(item =>
          <NavItem
            item={item}
            activeItem={activeItem}
            setActiveItem={setActiveItem}
            key={item.label}/>
        )}
      </ul>
    </nav>
  )
}
