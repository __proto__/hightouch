import xMark from "../../svgs/x-mark.svg";
import { NavBar }
  from "./Nav";
import React from "react";
import { Login, SignUp } from "../Buttons";

export const ModalNav = ({ navOpen, toggleNav, activeItem, setActiveItem }) => {
  let theme = ['bg-white', 'animated', 'fadeInDown']
  const fullScreenModal = 'fixed z-999 left-0 top-0 w-100 vh-100 overflow-y-auto'

  // gives time for user to see NavItem selected
  const delayedToggle = () => setTimeout(toggleNav, 400)

  return (
    <div
      className={navOpen ? `${fullScreenModal} ${theme.join(' ')}` : 'dn'}>
      <div className="mt2 mr1 pt3 pr3 flex justify-end" onClick={toggleNav}>
        <img src={xMark} className="" alt="X"/>
      </div>

      <NavBar
        activeItem={activeItem}
        setActiveItem={setActiveItem}
        navOpen={navOpen}
        toggleNav={delayedToggle}
      />

      <div className="mt3 flex justify-around">
        <SignUp extraClass={'w-40'}/>
        <Login extraClass={'w-40'}/>
      </div>
    </div>
  )
}
