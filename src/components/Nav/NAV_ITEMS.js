import home from "../../svgs/home.svg";
import destinations from "../../svgs/destinations.svg";
import audiences from "../../svgs/audiences.svg";
import login from "../../svgs/login.svg";
import shield from "../../svgs/shield.svg";

export const navItems = [
  { label: 'Home', svg: home },
  { label: 'Destinations', svg: destinations },
  { label: 'Audiences', svg: audiences },
  { label: 'Workspaces', svg: login },
  { label: 'Security', svg: shield },
]
