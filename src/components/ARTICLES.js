export const ARTICLES = [
  {
    img: 'img/Schedule_Syncs.png',
    alt: 'schedule hightouch syncs',
    heading: '3 great ways of scheduling a Hightouch sync',
    summary: 'We\'ve got three great ways of integrating Hightouch with your existing workflows: Airflow Operators, dbt Cloud integrations, and Webhook APIs.',
    link: '#'
  },
  {
    img: 'img/Data_Integration_The_Definitive_Guide_852b8c6bbb.png',
    alt: 'data integration: the definitive guide',
    heading: 'Data Integration: The Definitive Guide',
    summary: 'Getting data from your Data Warehouse into Slack couldn’t be easier. With just a few lines of SQL, you can start sending custom notifications to your teams in minutes.',
    link: '#'
  }
]
