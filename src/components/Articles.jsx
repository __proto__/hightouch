import React, { useEffect, useState } from "react";
import { ARTICLES } from "./ARTICLES";

export const Articles = () => {
  const [articles, setArticles] = useState([])

  useEffect(() => {
    const fetchArticles = async () => {
      const a = await Promise.resolve(ARTICLES)

      setArticles(a)
    }

    fetchArticles().catch(console.error)
  }, [])

  return (
    <div className='flex flex-wrap justify-around'>
      {articles.map(conf => (
        <article className="w-50-l mw6-l ph2 pv3" key={conf.heading}>
          <div className="ba b--light-gray br3 pa3 shadow-hover grow">
            <div className="mb2 near-white bg-dark-blue br3">
              <img className="br3 mw-100" src={conf.img} alt={conf.alt}/>
            </div>
            <h2 className="pt3 f5 fw6">{conf.heading}</h2>
            <p className="pt2 pb3 f6 fw4 measure-wide">{conf.summary}</p>
            <a href={conf.link} className="pt2 link gray">Read article →</a>
          </div>
        </article>
      ))}
    </div>
  )
}
